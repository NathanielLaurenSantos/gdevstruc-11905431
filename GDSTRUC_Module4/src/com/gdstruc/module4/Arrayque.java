package com.gdstruc.module4;

import java.util.NoSuchElementException;

public class Arrayque{
    private Player[] queue;
    private int front;
    private int back;

    public Arrayque (int capacity){
        queue = new Player[capacity];
    }

    public void add(Player player){
        if(back == queue.length){
            Player [] newArray = new Player[queue.length * 2];
            System.arraycopy(queue, 0, newArray, 0, queue.length);
            queue = newArray;
        }

        queue[back] = player;
        back ++;
    }

    public  Player remove(){
        if(size() == 0){
            throw new NoSuchElementException();
        }

        Player removePlayer = queue[front];
        queue[front] = null;
        front ++;
        return removePlayer;
    }

    public Player peek(){
        if(size() == 0){

            throw new NoSuchElementException();
        }
        return queue[front];
    }

    public int size(){
        return back - front;
    }

    public int getBack(){
        return back;
    }

    public void printQue(){
        for (int i = front; i < back; i++){
            System.out.println(queue[i]);
        }

    }
}
