package com.gdstruc.module4;

public class Main {

    public static void main(String[] args) {
        Arrayque playerQue = new Arrayque(5);
        int playerId= 1;
        int gameInProgress= 0;
        while (gameInProgress < 10) {

            for (int i = 0; i < rng(7); i++) {
                playerQue.add(new Player("Player", playerId));
                playerId++;
            }


            if (playerQue.size() >= 5) {
                gameInProgress++;
                for (int i = 0; i < 5; i++) {
                    playerQue.remove();
                }
            }

            System.out.println("Games in progress " + gameInProgress);
            System.out.println("Players current in queue: ");
            playerQue.printQue();
            pressEnter();
        }
        System.out.println("Max queue limit reached, Please try queueing again later");
    }

    //To generate a random number taken from https://www.javatpoint.com/how-to-generate-random-number-in-java 1st method
    public static int rng(int max){
        int min = 2;
        int result =  (int)(Math.random()*(max-min+1)+min);
        return result;
    }

    public static void pressEnter()
    {
        System.out.println("Press Enter key to continue...");
        try
        {
            System.in.read();
        }
        catch(Exception e)
        {}
    }

}
