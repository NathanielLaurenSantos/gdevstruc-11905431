package com.gdstruc.module5;

public class Main {

    public static void main(String[] args) {
	    Player azerea = new Player("Azerea", 80);
        Player myna = new Player("Myna", 213);
        Player chappy = new Player("Chappy", 511);
        Player gale = new Player("Gale", 70);

        SimpleHashtable hashtable = new SimpleHashtable();
        hashtable.put(azerea.getName(), azerea);
        hashtable.put(myna.getName(), myna);
        hashtable.put(chappy.getName(), chappy);
        hashtable.put(gale.getName(), gale);

        hashtable.printHashtable();
        System.out.println("\n\nRemoving player:");
        System.out.println(hashtable.remove("Gale"));
        System.out.println("\n\n");
        hashtable.printHashtable();
    }
}
