package com.gdstruc.module5;

public class SimpleHashtable {
    private  StoredPlayer[] hashtable;

    public SimpleHashtable(){
        hashtable = new StoredPlayer[10];
    }

    public void put (String Key, Player value){
        int hashedKey = hashKey(Key);

        if(isOccupied(hashedKey)){
            int stoppingIndex = hashedKey;
            if(hashedKey == hashtable.length -1){
                hashedKey = 0;
            }
            else {
                hashedKey++;
            }
            while (isOccupied(hashedKey) && hashedKey != stoppingIndex){
                hashedKey = (hashedKey + 1) % hashtable.length;
            }
        }

        if(isOccupied(hashedKey)){
            System.out.println(hashedKey + " Element is already occupied");
        }
        else{
            hashtable[hashedKey] = new StoredPlayer(Key, value);
        }
    }

    public Player get(String Key){
        int hashedKey = findKey(Key);
        if (hashedKey == -1){
            System.out.println("This player does not exist in our system");
            return null;
        }
        return hashtable[hashedKey].value;
    }

    public Player remove(String Key){
        int hashedKey = findKey(Key);

        if (hashedKey == -1){
            System.out.println("This player does not exist in our system");
            return null;
        }
        Player removedPlayer = hashtable[hashedKey].value;
        hashtable[hashedKey] = null;
        return removedPlayer;
    }

    private int hashKey(String key){
        return key.length() % hashtable.length;
    }

    private boolean isOccupied(int Index){
        return hashtable[Index] != null;
    }

    private int findKey(String key){
        int hashedKey = hashKey(key);
        if(hashtable[hashedKey] != null && hashtable[hashedKey].key.equals(key)){
            return hashedKey;
        }

        int stoppingIndex = hashedKey;
        if(hashedKey == hashtable.length -1){
            hashedKey = 0;
        }
        else {
            hashedKey++;
        }
        while (hashedKey != stoppingIndex && hashtable[hashedKey] != null && !hashtable[hashedKey].key.equals(key)){
            hashedKey = (hashedKey + 1) % hashtable.length;
        }
        if(hashtable[hashedKey] !=null && hashtable[hashedKey].key.equals(key)){
            return hashedKey;
        }

            return -1;
    }

    public void  printHashtable(){
        for (int i=0; i < hashtable.length; i++){
            System.out.println("Element " + i + " " + hashtable[i]);
        }
    }
}
