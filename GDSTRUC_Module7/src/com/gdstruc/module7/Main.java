package com.gdstruc.module7;

public class Main {

    public static void main(String[] args) {

        Tree tree = new Tree();

        tree.insert(50);
        tree.insert(12);
        tree.insert(34);
        tree.insert(66);
        tree.insert(40);
        tree.insert(61);
        tree.insert(42);
        tree.insert(31);
        tree.insert(96);
        tree.insert(7);
        tree.insert(29);

        //tree.traverseInOder();

        //Get Min value
        //System.out.println(tree.getMin());

        //Get Max Value
        //System.out.println(tree.getMax());

        //Print in Descending Orders
        tree.traverseInDescendingOder();
    }
}
