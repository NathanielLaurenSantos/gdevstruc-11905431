package com.gdstruc.midterms;

import java.util.LinkedList;
import java.util.ListIterator;

    public class ListClass {
        private LinkedList<Card> stack;
        private int size = 0;

        public ListClass() {
          stack = new LinkedList<Card>();
        }

        public void push (Card card){
            stack.push(card);
            size ++;
        }
        public boolean isEmpty(){
            return stack.isEmpty();
        }
        public Card pop(){
            size--;
            return stack.pop();
        }
        public Card peek(){
            return stack.peek();
        }

        public int getSize() {
            return size;
        }

        public void setSize(int size) {
            this.size = size;
        }

        public void printStack(){
            ListIterator<Card> iterator = stack.listIterator();
            System.out.println("Printing Stack:");
            while(iterator.hasNext()){
                System.out.println(iterator.next());
            }
        }
    }


