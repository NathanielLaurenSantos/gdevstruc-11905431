package com.gdstruc.midterms;

import java.util.LinkedList;

public class Main {

    public static void main(String[] args) {
        ListClass deckList = initializeDeck();
        ListClass handList = new ListClass();
        ListClass graveyardList = new ListClass();

        while(deckList.getSize() != 0){
            round(deckList, handList, graveyardList);
            System.out.println("Pause for 5 Seconds...");
            pause(5);
            System.out.flush();
        }

    }

    //To generate a random number taken from https://www.javatpoint.com/how-to-generate-random-number-in-java 1st method
    public static int rng(int max){
        int min = 1;
        int result =  (int)(Math.random()*(max-min+1)+min);
        return result;
    }

    public static void draw(ListClass deck, ListClass hand){
        int cardDraw = rng(5);
        for(int i = 0; i < cardDraw; i++) {
            hand.push(deck.peek());
            deck.pop();
        }
    }

    public static void discard (ListClass hand, ListClass graveyard){

        if (hand.isEmpty()==true){
            System.out.println("There is nothing to discard for your hand is empty");
        }
        else {
            int cardDiscard = rng(hand.getSize());
            for (int i = 0; i < cardDiscard; i++) {
                graveyard.push(hand.peek());
                hand.pop();
            }
        }
    }

    public static void revive (ListClass hand, ListClass graveyard){

        if(graveyard.isEmpty()==true){
            System.out.println("There are no cards to revive for the graveyard is empty");
        }
        else{
            int cardsToRevive = rng(graveyard.getSize());
            for(int i = 0; i < cardsToRevive; i++){
                hand.push(graveyard.peek());
                graveyard.pop();
            }

        }
    }

    public static void round(ListClass deck, ListClass hand, ListClass graveyard){
        int action = rng(3);
        if (action == 1){
            draw(deck, hand);
        }
        else if (action == 2){
            discard(hand, graveyard);
        }
        else if (action == 3){
            revive(hand,graveyard);
        }
        System.out.println("Deck size: " + deck.getSize());
        System.out.println("Hand size: " + hand.getSize());
        System.out.println("Graveyard size: " + graveyard.getSize());
        System.out.println("Cards Currently in hand:\n");
        hand.printStack();
    }

    public static ListClass initializeDeck(){
        ListClass deck = new ListClass();

        for (int i =  0; i <= 5; i++) {
            deck.push(new Card("Thundering Sword Dragon, AngerBlader"));
            deck.push(new Card("Little Battler, Giganoblazer"));
            deck.push(new Card("Ancient Emperor, GaiaEmperor"));
            deck.push(new Card("Ancient Dragon, Tyranolegend"));
            deck.push(new Card("Ancient Dragon, Spinodriver"));
            deck.push(new Card("Military Dragon, Raptor Colonel"));
        }
        return deck;
    }

    //Function to Pause the Program to read the Information
    public static void pause(int seconds){
        try {
            Thread.currentThread().sleep(seconds * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
