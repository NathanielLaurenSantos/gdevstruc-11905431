package com.company;

public class Main {

    public static void main(String[] args) {
	    int[] elements = new int[10];
        elements[0] = 39;
        elements[1] = 2;
        elements[2] = 47;
        elements[3] = -8;
        elements[4] = -19;
        elements[5] = -31;
        elements[6] = 7;
        elements[7] = 28;
        elements[8] = 50;
        elements[9] = -43;

        System.out.println("Before Sorting:");
        printArray(elements);

        //Just uncomment and comment the other to see the other item.
        //1.
        //bubbleSort(elements);

        //2
        selectionSort(elements);

        System.out.println("\n\nAfter Sorting:");
        printArray(elements);
    }

    private static void bubbleSort(int[] arr){
        //lsi = last sorted index
        for(int lsi = arr.length - 1; lsi > 0; lsi--){
            for(int i = 0; i<lsi;i++){
                if(arr[i] < arr[i+1]){
                    int temp = arr[i];
                    arr[i]= arr[i+1];
                    arr[i+1] = temp;
                }
            }
        }

    }

    private static void selectionSort(int[] arr){
        //lsi = last sorted index
        for(int lsi = arr.length-1; lsi>0; lsi--){
            int smallestIndex=0;
            for(int i=1;i<=lsi;i++){
                if(arr[i] < arr[smallestIndex]){
                    smallestIndex = i;
                }
            }
            int temp = arr[lsi];
            arr[lsi] = arr[smallestIndex];
            arr[smallestIndex] = temp;
        }
    }

    private static void printArray(int[] arr){
        for(int i = 0; i< arr.length; i++){
            System.out.print(arr[i] + " ");
        }

    }
}
