package com.company;

public class Main {

    public static void main(String[] args) {
	Player gwalin = new Player(2, "Gwalin", 15);
    Player destiny = new Player(7, "Destiny", 30);
    Player curex = new Player(18, "Curex", 10);

    PlayerLinkList linkList = new PlayerLinkList();
    linkList.addToFront(gwalin);
    linkList.addToFront(destiny);
    linkList.addToFront(curex);


    //Uncomment to see result
    //1. remove first element/front function
    //linkList.removeFront();
    linkList.printList();

    //3.a Uncomment to use the contains function inside linkList
//    if(linkList.contains(new Player(55, "Basher", 102)) == true) {
//       System.out.println("\n\nThis list contains that player");
//    }
//    else{
//        System.out.println("\n\nThis list does not contain that player");
//    }

    //3.b Uncomment to use the index of function inside linkList
//    System.out.print("\n\nThe index of the Player you're looking for is:\n");
//    System.out.print(linkList.indexOf(gwalin));
//
//    }
}
