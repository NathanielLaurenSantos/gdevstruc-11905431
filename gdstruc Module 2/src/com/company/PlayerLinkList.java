package com.company;

public class PlayerLinkList {
    private PlayerNode head;
    //Size Variable
    private int size = 0;

    public void addToFront(Player player){
        PlayerNode playerNode = new PlayerNode(player);
        playerNode.setNextPlayer(head);
        head = playerNode;

        //Adds a count to the size variable
        size ++;
    }

    public void removeFront(){
        PlayerNode current = head;
        PlayerNode temp = current.getNextPlayer();
        head = temp;

        //Removes a count to the variable
        size--;
    }

    boolean contains(Player player){
        PlayerNode current = head;
        while(current!=null) {
            if(current.getPlayer().getId() == player.getId() ) {
                return true;
            }
            current= current.getNextPlayer();
        }
        return false;
    }

    int indexOf(Player player){
        PlayerNode current = head;
        int index = 0;
        while(current!=null) {
            if(current.getPlayer().getId() == player.getId() ) {
                return index;
            }
            current= current.getNextPlayer();
            index++;
        }
        return -1;
    }

    public void printList(){
        PlayerNode current = head;
        System.out.print("Head -> ");
        while(current!=null){
            System.out.print(current.getPlayer());
            System.out.print(" -> ");
            current = current.getNextPlayer();
        }
        System.out.print("null");
        System.out.print("\n\nSize of the List: \n");
        System.out.print(size);
    }
}
